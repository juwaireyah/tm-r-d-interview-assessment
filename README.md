# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary


This is the interview assessment from TM R&D. The assessment is all about a web application that displays top 5 websites based on the selected date. 


### How do I get set up? ###

* Summary of set up


The application has been developed using XAMPP. Assuming you have XAMPP installed, please do the follwoing steps.


1. Clone the repository and place it under the folder C:\xampp\htdocs with the folder name "test".
2. To enable Postgres in XAMPP, we need to perform the followings to get PHP communicating with PostgreSQL.


        * Open php.ini file located in C:\xampp\php
        * Uncommment the following lines in php.ini.
                  ** extension=php_pdo_pgsql.dll**
                   **extension=php_pgsql.dll**
        * Restart the Apache server.

3. Import the sql file "DatasetDB.sql" in the repository to PosgresSQL by using pgAdmin. To import, follow those steps. 


         * Create a database in pgAdmin with the name "postgres".
         * Create a table named "webrank" with 4 columns; ID,date,website,visits where ID 
           is primary key.
         * Right Click on the table and click Import/Export.
         * Select Import.
         * In the file info, browse the sql file where it is located and select it. When 
           browsing, select "All file" for format.
         * In file info, Select "text" for format.
         * Click ok. The data has been imported to your database now!!

4. Done! You can run the application at http:\\localhost:8080\test\dataSelect.html now!